'use strict';

var gat = require('gulp-auto-task');
var gulp = require('gulp');

module.exports = gulp.parallel(
    gat.load('lint/eslint')
);
