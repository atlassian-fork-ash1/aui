const proc = require('child_process');
const soyJarPath = require.resolve('@atlassian/soy-template-plugin-js/src/jar/atlassian-soy-cli-support.jar');

function toKeyVal (obj) {
    var parts = [];

    for (var a in obj) {
        if (obj.hasOwnProperty(a)) {
            parts.push(`${a}:${obj[a]}`);
        }
    }

    return parts.join(',');
}

function translateOption (opt) {
    if (typeof opt === 'object') {
        return toKeyVal(opt);
    }

    return opt;
}

const defaults = {
    jar: soyJarPath,
    args: {
        basedir: undefined,
        data: undefined,
        dependencies: undefined,
        glob: undefined,
        i18n: undefined,
        outdir: undefined,
        rootnamespace: undefined,
        type: 'js'
    }
};

module.exports = function soyShell (opts, cb) {
    opts = opts || {};
    const bin = process.env.JAVA_HOME ? `${process.env.JAVA_HOME}/bin/java` : 'java';
    const parts = [
        '-jar', (opts.jar || defaults.jar)
    ];

    for (let a in defaults.args) {
        if (defaults.args.hasOwnProperty(a)) {
            var value = opts.args && opts.args[a] || defaults.args[a];

            if (value !== undefined) {
                parts.push(`--${a}`);
                parts.push(translateOption(value));
            }
        }
    }

    console.log('Running soy with the following commands:', bin, parts);

    if (typeof cb === 'function') {
        const cmd = proc.spawn(bin, parts);
        const stdout = [], stderr = [];
        cmd.stdout.on('data', data => { str = String(data).trim(); stdout.push(str); console.log(str); });
        cmd.stderr.on('data', data => { str = String(data).trim(); stderr.push(str); console.error(str); });
        cmd.on('close', exit => cb(exit, stdout.join('\n'), stderr.join('\n')));
    } else {
        const cmd = proc.spawn(bin, parts);
        console.log(String(cmd.stdout));
        console.log(String(cmd.stderr));
        return cmd.exit;
    }
};
