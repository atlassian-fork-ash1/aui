/// <reference types="Cypress" />
/* eslint-disable */

const variants = {
    'experimental/avatar/sizes/': ['#content'],
    'auiBadge/': ['#content'],
    'experimental/buttons/': ['#content'],
    'i18n/fontStacks/': ['#content'],
    'icons/': ['#content'],
    'experimental/lozenges/': ['#content'],
    'experimental/pageLayout/layouts/navigation/default/': ['#content'],
    'experimental/pageLayout/layouts/groups/': ['#content'],
    'experimental/pageLayout/header/auiHeader/': ['#nav1', '#nav2', '#nav3', '#nav4'],
    'experimental/pageLayout/header/pageHeaderVariations/': ['#content'],
    'tables/': ['#basic', '#nested'],
    'toolbar/': ['#content'],
    'experimental/toolbar2/': ['#content']
};

Object.entries(variants).forEach(([path, selectors]) => {
    context(path, () => {
        const pathPrefix = path.replace(/\//g, '_');

        beforeEach(() => {
            cy.visit(path);
        });

        selectors.forEach(selector => {
            it(selector, () => {
                cy.get(selector).matchImageSnapshot(`${pathPrefix}_${selector}`)
            })
        });
    });
});

it('experimental/datePicker', () => {
    const path = 'experimental/datePicker/';
    const selector = '.aui-datepicker-dialog[aria-hidden="false"]';
    const pathPrefix = path.replace(/\//g, '_');
    cy.visit('experimental/datePicker/');
    cy.get('#test-default-always').click();
    cy.get(selector).first().matchImageSnapshot(`${pathPrefix}_${selector}`)
});

it('inlineDialog2', () => {
    const path = 'inlineDialog2/';
    const selector = 'body';
    const pathPrefix = path.replace(/\//g, '_');
    cy.visit('inlineDialog2/');
    cy.get('a[aria-controls="inline-dialog2-17"]').click();
    cy.get('a[aria-controls="inline-dialog2-16"]').click();
    cy.get('a[aria-controls="inline-dialog2-help-3"]').click();
    cy.get(selector).first().matchImageSnapshot(`${pathPrefix}_${selector}`);
});
