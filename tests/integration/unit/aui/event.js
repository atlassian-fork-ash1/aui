import * as events from '@atlassian/aui/src/js/aui/event';
import _ from 'underscore';

describe('aui/event', function () {
    it('global', function () {
        _.forEach(events, function(item, key) {
            expect(AJS[key].toString()).to.equal(item.toString());
        });
    });

    it('Binding', function () {
        var spy = sinon.spy();

        events.bind('test1-event', spy);
        events.trigger('test1-event');
        spy.should.have.been.calledOnce;
    });

    it('Unbinding', function () {
        var spy = sinon.spy();

        events.bind('test2-event', spy);
        events.trigger('test2-event');
        spy.should.have.been.calledOnce;

        events.unbind('test2-event');
        events.trigger('test2-event');
        spy.should.have.been.calledOnce;
    });
});
