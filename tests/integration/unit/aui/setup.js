import $ from '@atlassian/aui/src/js/aui/jquery';
import '@atlassian/aui/src/js/aui/setup';

describe('aui/setup', function () {
    it('should globalize jQuery or Zepto to $', function () {
        expect(AJS.$.toString()).to.equal($.toString());
    });
});
