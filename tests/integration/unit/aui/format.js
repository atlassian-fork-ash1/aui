import format from '@atlassian/aui/src/js/aui/format';
import * as logger from '@atlassian/aui/src/js/aui/internal/log';

describe('aui/format', function () {
    it('globals', function () {
        expect(AJS.format.toString()).to.equal(format.toString());
    });

    it('with 1 parameter', function () {
        var testFormat = format('hello {0}', 'world');
        expect(testFormat).to.equal('hello world');
    });

    it('with 2 parameters', function () {
        var testFormat = format('hello {0} {1}', 'world', 'again');
        expect(testFormat).to.equal('hello world again');
    });

    it('with 3 parameters', function () {
        var testFormat = format('hello {0} {1} {2}', 'world', 'again', '!');
        expect(testFormat).to.equal('hello world again !');
    });

    it('with 4 parameters', function () {
        var testFormat = format('hello {0} {1} {2} {3}', 'world', 'again', '!', 'test');
        expect(testFormat).to.equal('hello world again ! test');
    });

    it('with symbols', function () {
        var testFormat = format('hello {0}', '!@#$%^&*()');
        expect(testFormat).to.equal('hello !@#$%^&*()');
    });

    it('with curly braces', function () {
        var testFormat = format('hello {0}', '{}');
        expect(testFormat).to.equal('hello {}');
    });

    it('with repeated parameters', function () {
        var testFormat = format('hello {0}, {0}, {0}', 'world');
        expect(testFormat).to.equal('hello world, world, world');
    });

    it('with apostrophe', function () {
        var testFormat = format('hello \'{0}\' {0} {0}', 'world');
        expect(testFormat).to.equal('hello {0} world world');
    });

    it('with very long parameters', function () {
        var testFormat = format('hello {0}', new Array(25).join('this parameter is very long ')); // we're joining 25 empty values together, which means we'll get our join string 24 times.
        expect(testFormat).to.equal('hello this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long '); // eslint-disable-line
    });

    // positional equivalence
    it('with prefix', function () {
        var testFormat = format('{0} two three', 'one');
        expect(testFormat).to.equal('one two three');
    });

    it('with infix', function () {
        var testFormat = format('one {0} three', 'two');
        expect(testFormat).to.equal('one two three');
    });

    it('with postfix', function () {
        var testFormat = format('one two {0}', 'three');
        expect(testFormat).to.equal('one two three');
    });

    it('with differing order', function () {
        var testFormat = format('{2} {0} {1}', 'two', 'three', 'one');
        expect(testFormat).to.equal('one two three');
    });

    // choices
    it('with a choice value missing parameter', function () {
        var testFormat = format('We got {0,choice,0#|1#1 issue|1<{1,number} issues}');
        expect(testFormat).to.equal('We got ');
    });

    it('with a choice value with parameter lower first option', function () {
        var testFormat = format('We got {0,choice,0#0 issues|1#1|1<{1,number} issues}', -1, -1);
        expect(testFormat).to.equal('We got 0 issues');
    });

    it('with a choice value first option', function () {
        var testFormat = format('We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}', 0);
        expect(testFormat).to.equal('We got 0 issues');
    });

    it('with a choice value middle option', function () {
        var testFormat = format('We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}', 1);
        expect(testFormat).to.equal('We got 1 issue');
    });

    it('with a choice value last option', function () {
        var testFormat = format('We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}', 2);
        expect(testFormat).to.equal('We got 2 issues');
    });

    it('with a choice value with missing number parameter option', function () {
        var testFormat = format('We got {0,choice,0# |1#1 issue|1<{1,number} issues}', 2);
        expect(testFormat).to.equal('We got  issues');
    });

    it('with a choice value with valid second option', function () {
        var testFormat = format('We got {0,choice,0# |1#1 issue|1<{1,number} issues}', 10, 10);
        expect(testFormat).to.equal('We got 10 issues');
    });

    // number
    it('with a number value', function () {
        var testFormat = format('Give me {0,number}!', 5);
        expect(testFormat).to.equal('Give me 5!');
    });

    it('with a floating point value', function () {
        var testFormat = format('Give me {0,number}!', Math.PI);
        expect(testFormat).to.equal('Give me 3.141592653589793!');
    });

    it('with a number value and missing parameter', function () {
        var testFormat = format('Give me {0,number}!');
        expect(testFormat).to.equal('Give me !');
    });

    /** @note Not implemented according to jsdoc */
    xit('with a number value and non-numeric parameter', function () {
        var testFormat = format('Give me {0,number}!', 'everything');
        expect(testFormat).to.equal('Give me !');
    });

    it('with a bit of everything', function() {
        var issueChoice = '{0,choice,0#0 zgłoszeń|1#1 zgłoszenia|4<{0,number} zgłoszenie|5<{0,number} zgłoszeń}';
        var timeChoice = '{1,choice,0#brak czasu|1#1 minuty|4<{1,number} minut|5<{1,number} minut}';
        var goodLuck = 'powodzenia';
        // reads: try to solve x issues within y minutes or less. good luck!
        var testFormat = format(`spróbuj rozwiązać ${issueChoice} w ciągu ${timeChoice} lub szybciej. {2}!`, 4, 12, goodLuck);
        expect(testFormat).to.equal('spróbuj rozwiązać 4 zgłoszenie w ciągu 12 minut lub szybciej. powodzenia!');
    });

    describe('with invalid choicePart formats', () => {
        let errorStub;

        beforeEach(function () {
            errorStub = sinon.stub(logger, 'error');
        });

        afterEach(function () {
            errorStub.restore();
        });

        var invalidFormatToErrorMap = {
            'approval approvals: {0} {0,choice,1#|1<}': 'The format "0,choice,1#|1<" from message "approval approvals: {0} {0,choice,1#|1<}" is invalid.',
            'approval approvals: {0} {0,choice,1|1<}': 'The format "0,choice,1|1<" from message "approval approvals: {0} {0,choice,1|1<}" is invalid.',
            'approval approvals: {0} {0,choice,1}': 'The format "0,choice,1" from message "approval approvals: {0} {0,choice,1}" is invalid.',
        };
        var formatCall = invalidFormat => () => format(invalidFormat, 1, 5, 7, 9, 11);

        Object.keys(invalidFormatToErrorMap).forEach(invalidFormat => {
            it('should not throw exception for invalid format "' + invalidFormat + '"', () => {
                expect(formatCall(invalidFormat)).to.not.throw();
            });

            it('should log error for invalid format "' + invalidFormat + '"', () => {
                var error = invalidFormatToErrorMap[invalidFormat];

                format(invalidFormat, 1);

                expect(logger.error.called).to.be.true;
                expect(logger.error.lastCall.args[0]).to.equal(error);
            });
        });
    });
});
