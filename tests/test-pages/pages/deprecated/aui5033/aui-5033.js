(function() {
    var datepickerHtml = '<input type="date" class="show-a-datepicker" />';
    var ddHtml = `<ul id="show-a-dropdown" class="styled-parent">
    <li class="aui-dd-parent">
        <button class="aui-button aui-dd-trigger">Show me an old Dropdown</button>
        <ul class="aui-dropdown">
            <li class="dropdown-item"><a href="http://www.google.com" class="item-link">Don't make more!</a></li>
            <li class="dropdown-item"><a href="http://example.com/" class="item-link">Don't make more!</a></li>
            <li class="dropdown-item"><a href="http://example.com/" class="item-link">Don't make more!</a></li>
            <li class="dropdown-item"><a href="http://example.com/" class="item-link">Don't make more!</a></li>
            <li class="dropdown-item"><a href="http://example.com/" class="item-link">Don't make more!</a></li>
        </ul>
    </li>
</ul>`;

    AJS.$(function() {
        var d = new AJS.Dialog();
        d.addPanel('Datepicker', `<p>A datepicker should appear above the dialog</p>${datepickerHtml}`, 'panel-body');
        d.addPanel('Dropdown1', `<p>A dropdown should appear above the dialog</p>${ddHtml}`, 'panel-body');

        var datepicker;
        var dropdown1;

        AJS.$(document).on('click', '#press-me', function () {
            d.gotoPage(0);
            d.gotoPanel(0);
            d.show();

            if (!datepicker) {
                datepicker = AJS.$('.show-a-datepicker').datePicker({
                    overrideBrowserDefault: true
                });
            }

            if (!dropdown1) {
                dropdown1 = AJS.$('#show-a-dropdown').dropDown('Standard');
            }
        });
    });
}());
