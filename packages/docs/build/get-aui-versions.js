const request = require('request');
const { name: auiPkgName } = require('../../core/package.json');

const STABLE_REG_EXP = /^\d+\.\d+\.\d+$/;
const MINOR_REG_EXP = /^\d+\.\d+/;

const matchStableVersion = version => version.match(STABLE_REG_EXP);
const getMinorVersionPart = version => version.match(MINOR_REG_EXP).pop();

function versionComparator(a, b) {
    let [majorA, minorA, patchA] = a.split('.');
    let [majorB, minorB, patchB] = b.split('.');

    if (majorA !== majorB) {
        return majorA - majorB;
    }

    if (minorA !== minorB) {
        return minorA - minorB;
    }

    return patchA - patchB;
}

function getStableVersions(versions) {
    return versions.filter(matchStableVersion);
}

function getLatestStableVersions(versions) {
    return versions.filter((version, i) => {
        const nextVersion = versions[i + 1];

        if (nextVersion) {
            return getMinorVersionPart(version) !== getMinorVersionPart(nextVersion);
        }

        return true;
    });
}

module.exports = function libGetAuiVersions() {
    return new Promise((resolve, reject) => {
        request(`https://registry.npmjs.com/${auiPkgName.replace('/', '%2F')}`, { json: true }, (err, resp, json) => {
            if (err) {
                return reject(err);
            }

            const allVersions = Object.keys(json.versions).sort(versionComparator);
            const versionReleaseDates = allVersions.reduce((map, version) => {
                map[version] = json.time[version];
                return map;
            }, {});
            const stableVersions = getStableVersions(allVersions).reverse();
            const latestVersions = getLatestStableVersions(stableVersions);

            resolve({
                all: versionReleaseDates,
                latest: latestVersions,
                stable: stableVersions,
            });
        });
    });
};
