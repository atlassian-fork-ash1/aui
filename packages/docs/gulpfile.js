/* eslint-env node */
const fs = require('fs');
const del = require('del');
const gulp = require('gulp');
const less = require('gulp-less');
const gulpWebserver = require('gulp-webserver');
const argv = require('yargs').argv;

const docsOpts = require('./build/docs.opts');

const buildWebpack = require('./build/docs.webpacker');
const buildMetalsmith = require('./build/docs.metalsmith');
const libGetAuiVersions = require('./build/get-aui-versions');

//
// Helper functions and data used when building
//

const processVersions = (opts) => function processAuiVersions(done) {
    const directoryPath = opts;
    libGetAuiVersions()
        .then(versions => JSON.stringify(versions))
        .then(data => {
            fs.mkdirSync(directoryPath, { recursive: true });
            fs.writeFileSync(directoryPath + '/versions.json', data, {encoding: 'utf8', flags: 'w'});
        })
        .then(() => done());
};

const runWebserver = (opts) => function docsServer() {
    opts = Object.assign({
        host: docsOpts.host,
        port: docsOpts.port,
        open: docsOpts.path
    }, opts);
    return gulp.src('dist')
        .pipe(gulpWebserver(opts));
};

let frontendOpts = {};
let backendOpts = {
    docsVersion: argv.docsVersion
};

const dev = (isDev) => function setDevmode(done) {
    if (isDev) {
        frontendOpts.watch = true;
        backendOpts.watch = true;
    }
    done();
};

//
// Actual Gulp tasks
//

const clean = (done) => del(['.tmp', 'dist']).then(() => done());
const build = gulp.series(clean, gulp.parallel(
    function buildFrontend(done) {
        buildWebpack(frontendOpts)(done)
    },
    function buildBackend(done) {
        buildMetalsmith.docs(backendOpts)(done)
    }
), processVersions('./dist/aui'));
const run = gulp.series(dev(false), build, runWebserver({livereload: false}));
const watch = gulp.series(dev(true), build, runWebserver({livereload: false}));

const copyImages = done => {
    gulp.src('./index-page/images/**').pipe(gulp.dest('./dist-index-page/assets/images'));
    done();
};
const createHTML = done => buildMetalsmith.indexPage(backendOpts)(done);
const processStyles = done => {
    gulp.src('./index-page/src/styles/*.less')
        .pipe(less())
        .pipe(gulp.dest('./dist-index-page/styles'));
    done();
};

const buildIndex = gulp.series(
    clean,
    copyImages,
    createHTML,
    processStyles,
    processVersions('./dist-index-page/aui')
);

module.exports = {
    clean,
    build,
    run,
    watch,
    buildIndex
};
