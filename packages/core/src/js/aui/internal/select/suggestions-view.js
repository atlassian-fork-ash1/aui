import $ from '../../jquery';
import { I18n } from '../../i18n';
import Alignment from '../alignment';
import layer from '../../layer';

function generateListItemID (listId, index) {
    return listId + '-' + index;
}

/**
 *
 * @param view SuggestionsView
 */
function enableAlignment (view) {
    if (view.anchor && !view._auiAlignment) {
        view._auiAlignment = new Alignment(view.el, view.anchor, {
            flipContainer: 'scrollParent',
            positionFixed: false,
            preventOverflow: false
        });
    }

    if (view._auiAlignment) {
        view._auiAlignment.enable();
        view._auiAlignment.scheduleUpdate();
    }
}

function disableAlignment (view) {
    if (view._auiAlignment) {
        view._auiAlignment.disable();
    }
}

function destroyAlignment (view) {
    if (view._auiAlignment) {
        view._auiAlignment.destroy();
        delete view._auiAlignment;
    }
}

function matchWidth (view) {
    $(view.el).css('min-width', $(view.anchor).outerWidth());
}

function SuggestionsView (element, anchor) {
    this.el = element;
    this.anchor = anchor;
}

function clearActive (element) {
    $(element).find('.aui-select-active').removeClass('aui-select-active');
}

SuggestionsView.prototype = {
    render: function (suggestions, currentLength, listId) {
        this.currListId = listId;
        var html = '';

        // Do nothing if we have no new suggestions, otherwise append anything else we find.
        if (suggestions.length) {
            var i = currentLength;
            suggestions.forEach(function (sugg) {
                var label = sugg.getLabel();
                var imageSrc = sugg.get('img-src');
                var image = imageSrc ? `<img src="${imageSrc}"/>` : '';
                var newValueText = sugg.get('new-value') ? (` (<em>${I18n.getText('aui.select.new.value')}</em>)`) : '';
                html += `<li role="option" class="aui-select-suggestion" id="${generateListItemID(listId, i)}">${image}${label}${newValueText}</li>`;
                i++;
            });

            // If the old suggestions were empty, a <li> of 'No suggestions' will be appended, we need to remove it
            if (currentLength) {
                this.el.querySelector('ul').innerHTML += html;
            } else {
                this.el.querySelector('ul').innerHTML = html;
            }
        } else if (!currentLength) {
            this.el.querySelector('ul').innerHTML = `<li role="option" class="aui-select-no-suggestions">${I18n.getText('aui.select.no.suggestions')}</li>`;
        }

        return this;
    },
    setActive: function (active) {
        clearActive(this.el);
        $(this.el).find('#' + generateListItemID(this.currListId, active)).addClass('aui-select-active');
    },
    getActive: function () {
        return this.el.querySelector('.aui-select-active');
    },
    show: function () {
        matchWidth(this);
        layer(this.el).show();
        enableAlignment(this);
    },
    hide: function () {
        clearActive(this.el);
        layer(this.el).hide();
        disableAlignment(this);
    },
    destroy: function () {
        this.hide();
        destroyAlignment(this);
        delete this.currListId;
    },
    isVisible: function () {
        return $(this.el).is(':visible');
    }
};

export default SuggestionsView;
