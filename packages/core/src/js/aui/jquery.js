import $ from 'jquery';
// We provide the value from the import statement last, just in case jQuery or Zepto have been called with noConflict previously.
export default window.jQuery || window.Zepto || $;
