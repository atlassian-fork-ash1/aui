import globalize from './internal/globalize';

// @note: this value is set via webpack and gulp
// eslint-disable-next-line
var version = AUI_VERSION;

globalize('version', version);

export default version;
