// The file sizes below are rounded up by 0.01KB from existing sizes
// so that if any one of them grows significantly we will know.

const files = {
    'aui-css-deprecations.js': '10KB',
    'aui-prototyping.css': '235.86KB',
    'aui-prototyping.js': '491.78KB',
    'aui-prototyping.nodeps.css': '235.86KB',
    'aui-prototyping.nodeps.js': '451.05KB',

    'fonts/adgs-icons.eot': '52.86KB',
    'fonts/adgs-icons.ttf': '52.69KB',
    'fonts/adgs-icons.woff': '24.83KB',
    'fonts/atlassian-icons.eot': '44.16KB',
    'fonts/atlassian-icons.ttf': '43.97KB',
    'fonts/atlassian-icons.woff': '58.39KB',

    'images/adgs-icons.svg': '624.29KB',
    'images/atlassian-community-white.svg': '5.44KB',
    'images/atlassian-developer-white.svg': '5.48KB',
    'images/atlassian-horizontal-blue.svg': '2.64KB',
    'images/atlassian-horizontal-neutral.svg': '2.65KB',
    'images/atlassian-horizontal-white.svg': '2.63KB',
    'images/atlassian-icons.svg': '105.21KB',
    'images/atlassian-marketplace-white.svg': '6.07KB',
    'images/atlassian-partner-program-white.svg': '5.82KB',
    'images/atlassian-support-white.svg': '5.14KB',
    'images/atlassian-university-white.svg': '4.53KB',
    'images/bamboo-white.svg': '5.15KB',
    'images/bitbucket-white.svg': '5.59KB',
    'images/clover-white.svg': '2.20KB',
    'images/confluence-white.svg': '6.87KB',
    'images/crowd-white.svg': '4.02KB',
    'images/crucible-white.svg': '4.79KB',
    'images/fisheye-white.svg': '4.75KB',
    'images/hipchat-white.svg': '4.71KB',
    'images/jira-core-white.svg': '4.27KB',
    'images/jira-service-desk-white.svg': '7.40KB',
    'images/jira-software-white.svg': '6.70KB',
    'images/jira-white.svg': '3.24KB',
    'images/jira-with-core-white.svg': '2.88KB',
    'images/jira-with-service-desk-white.svg': '3.13KB',
    'images/jira-with-software-white.svg': '3.67KB'
};

const bundles = Object.entries(files).map(([name, maxSize]) => ({
    name,
    maxSize
}));

module.exports = {
    bundles
};
